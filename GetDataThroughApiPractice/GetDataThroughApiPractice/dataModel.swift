//
//  dataModel.swift
//  GetDataThroughApiPractice
//
//  Created by mayank ranka on 05/04/23.
//

import UIKit

class dataModel: NSObject {

    var title               : String?
    var shortDescription    : String?
    var collectedValue      : Int?
    var startDate           : String?
    
    init(dictionary : [String : Any]){
        title               = dictionary["title"] as! String?
        shortDescription    = dictionary["shortDescription"] as! String?
        collectedValue      = dictionary["collectedValue"] as! Int?
        startDate           = dictionary["startDate"] as! String?
    }
}
