//
//  dataModelParser.swift
//  GetDataThroughApiPractice
//
//  Created by mayank ranka on 05/04/23.
//

import UIKit

class dataModelParser: NSObject, URLSessionDelegate, URLSessionDownloadDelegate {
    
    var webData : Data!
    var session : URLSession {
        let defaultConfig = URLSessionConfiguration.default
        defaultConfig.requestCachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
       let session1 = URLSession(configuration: defaultConfig, delegate: self, delegateQueue: nil)
        
        return session1
    }
    
    func getWebdata(){
        
        let url = URL(string: "https://testffc.nimapinfotech.com/testdata.json")
        let task = session.downloadTask(with: url!)
        task.resume()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        do{
            webData = try Data(contentsOf: location)
            
            let jsonData = try JSONSerialization.jsonObject(with: webData, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String : Any]
            let data = jsonData["data"] as! [String : Any]
            
            let records = data["Records"] as! [[String : Any]]
            var arrData = [dataModel]()
            for i in 0..<records.count{
                let user = dataModel(dictionary: records[i])
                arrData.append(user)
            }
            print(arrData)
        }catch{
            print("Error")
        }
        
    }
    

}
